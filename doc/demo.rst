Demo code
=========

Reading data from AIN
---------------------

.. literalinclude:: ../example/ain-snapshot-basic.py

Toggle pins using DIO
----------------------

.. literalinclude:: ../example/dio-pin0-toggle.py

Generate Constant voltage
--------------------------

.. literalinclude:: ../example/aout-constant-voltage.py

Controlling LED strip connected to DIO
---------------------------------------

.. literalinclude:: ../example/led_strip.py

Reading using ADS1220 ADC
-------------------------

.. literalinclude:: ../example/ads1220-print.py

AIN -> AOUT streaming
----------------------

.. literalinclude:: ../example/ain-aout-feed.py

.. Need work!

	AIN -> AOUT streaming (signal filtering)
	-----------------------------------------

	.. literalinclude:: ../example/ain-aout-filtered.py

Plotting AIN snapshot data with PyPlot
---------------------------------------

.. literalinclude:: ../example/ain_snapshot_pyplot.py

Plotting AIN snapshot data with PyQtGraph
------------------------------------------

.. literalinclude:: ../example/ain_snapshot_pyqtgraph.py

Plotting AIN stream data with PyQtGraph
----------------------------------------

.. literalinclude:: ../example/ain_stream_pyqtgraph.py

Small Power supply controlling program (via pyUSB)
---------------------------------------------------

.. literalinclude:: ../example/power-supply.py

PWM basic example
------------------

.. literalinclude:: ../example/pwm_example.py

Time varying PWM output
------------------------

.. literalinclude:: ../example/pwm_sweep.py

.. Need testing!

	Motor surge detector
	--------------------

	.. literalinclude:: ../example/motor_surge.py


	Small I2C program
	------------------

	.. literalinclude:: ../example/i2c-util.py
