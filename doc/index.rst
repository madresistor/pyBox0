.. pyBox0 documentation master file, created by
   sphinx-quickstart on Wed Sep  2 03:43:53 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyBox0's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   demo
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

